---
title: Certificates
subtitle: Approved Platzi courses (<a target="_blank" href="https://platzi.com/@henrytabimagiraldo">watch profile</a>).
layout: "page"
icon: fas fa-certificate
order: 3
---

{% for certificate in site.data.certificates %}
  <div class="certificate">
    <a target="_blank" href="{{ certificate.url }}">
      <img src="{{ certificate.img | relative_url }}" />
    </a>
    <h3>{{ certificate.title }}</h3>
    <p>{{ certificate.date }}</p>
  </div>
{% endfor %}
